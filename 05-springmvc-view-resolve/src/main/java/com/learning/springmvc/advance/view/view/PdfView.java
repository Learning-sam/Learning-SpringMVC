package com.learning.springmvc.advance.view.view;

import com.learning.springmvc.advance.view.bean.User;
import com.lowagie.text.BadElementException;
import com.lowagie.text.Cell;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Phrase;
import com.lowagie.text.Table;
import org.springframework.web.servlet.view.document.AbstractPdfView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * ClassName: PdfView
 * Description: PDF 视图
 * Date: 2019/5/23 11:10 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
public class PdfView extends AbstractPdfView {


    @Override
    protected void buildPdfDocument(Map<String, Object> model,
                                    com.lowagie.text.Document document, com.lowagie.text.pdf.PdfWriter writer,
                                    HttpServletRequest request, HttpServletResponse response) throws Exception {
        response.setHeader("Content-Disposition", "attachment;filename=" + new String("用户列表".getBytes(), "iso8859-1"));
        User user = (User) model.get("user");

        Table table = new Table(3);
        table.setWidth(80);
        table.setBorder(1);
        table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
        table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);

        table.addCell("name");
        table.addCell("mobile");
        table.addCell("birthday");
        table.addCell(user.getName());

        document.add(table);

    }

    private Cell buildFontCell(String content, Font font) throws BadElementException {
        Phrase phrase = new Phrase(content, font);
        return new Cell(phrase);
    }
}
