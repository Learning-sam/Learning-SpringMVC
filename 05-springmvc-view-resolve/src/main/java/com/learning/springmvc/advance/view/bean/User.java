package com.learning.springmvc.advance.view.bean;

import com.alibaba.fastjson.JSON;
import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

/**
 * ClassName: User
 * Description:
 * Date: 2016/12/22 21:02
 * <p>
 * 校验规则：
 * - @UserConstraint name 必须是eason
 * - name 属性不能为空
 * - mobile 在 Insert 分组时不能为空
 * - birthday 在 Select 分组时不能为空
 *
 * @author SAM SHO
 * @version V1.0
 */
@Setter
@Getter
@XmlRootElement(name = "user")
public class User {

    private String name;
    private String mobile;
    private Date birthday;
    private Date createDate;

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}
