package com.learning.springmvc.advance.view.controller;

import com.learning.springmvc.advance.view.bean.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * ClassName: UserController
 * Description:
 * Date: 2019/5/21 16:23 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Controller
@RequestMapping("user")
public class UserController {

    /**
     * JSP 视图解析器
     * <p>
     * http://localhost:8080/user/view/jsp?name=Sam
     *
     * @param user
     * @param model
     * @return
     */
    @GetMapping("view/jsp")
    public String jsp(User user, Model model) {
        model.addAttribute("user", user);
        return "jsp/user";
    }

    /**
     * freemarker 视图解析器
     * <p>
     * http://localhost:8080/user/view/ftl?name=Sam
     *
     * @param user
     * @param model
     * @return
     */
    @GetMapping("view/ftl")
    public String ftl(User user, Model model) {
        model.addAttribute("user", user);
        return "ftl/user";
    }

    /**
     * Excel 视图
     * http://localhost:8080/user/view/excel?name=Sam
     *
     * @param user
     * @param model
     * @return
     */
    @GetMapping("view/excel")
    public String excel(User user, Model model) {
        model.addAttribute("user", user);
        // 使用BeanNameViewResolver视图解析器，这边直接视图的Bean编号
        return "excelView";
    }

    /**
     * PDF 视图
     * http://localhost:8080/user/view/pdf?name=Sam
     *
     * @param user
     * @param model
     * @return
     */
    @GetMapping("view/pdf")
    public String pdf(User user, Model model) {
        model.addAttribute("user", user);
        // 使用BeanNameViewResolver视图解析器，这边直接视图的Bean编号
        return "pdfView";
    }

    /**
     * XML 视图
     * http://localhost:8080/user/view/xml?name=Sam
     *
     * @param user
     * @param model
     * @return
     */
    @GetMapping("view/xml")
    public String xml(User user, Model model) {
        model.addAttribute("user", user);
        // 使用BeanNameViewResolver视图解析器，这边直接视图的Bean编号
        return "xmlView";
    }

    /**
     * JSON 视图
     * http://localhost:8080/user/view/json?name=Sam
     *
     * @param user
     * @param model
     * @return
     */
    @GetMapping("view/json")
    public String json(User user, Model model) {
        model.addAttribute("user", user);
        // 使用BeanNameViewResolver视图解析器，这边直接视图的Bean编号
        return "jsonView";
    }


    /**
     * 混合视图解析器
     * http://localhost:8080/user/get?name=sam
     * http://localhost:8080/user/get.xml?name=sam
     * http://localhost:8080/user/get.json?name=sam
     *
     * @param user
     * @return
     */
    @GetMapping("get")
    public String contentNegotiating(User user, Model model) {
        model.addAttribute("user", user);
        return "jsp/user";
    }

}