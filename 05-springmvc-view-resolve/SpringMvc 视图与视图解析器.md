## Learning-SpringMVC

### 配置实现
1. XML
2. JavaConfig

### 常用功能
#### 控制器功能开发实现 
1. 使用 @RequestMapping 映射请求
- 通过请求URL进行映射
- 使用 URI Template Patterns 风格
- 通过请求参数、请求方法/请求头进行映射
- @RequestMapping请求方法组合注解

2. 请求处理方法参数绑定（参数获取）
- 使用@RequestParam绑定请求参数
- 使用@CookieValue绑定请求中的Cookie值
- 使用 @RequestHeader 绑定请求报文头的属性值
- 使用参数/表单对象绑定请求参数值
- 使用 Servlet API 对象作为入参
- 使用IO对象作为入参
- 使用矩阵变量绑定参数
- 其他类型的参数
3. 处理模型数据
- ModelAndView
- Model、ModelMap、Map
- @ModelAttribute
- @SessionAttributes
- 转发与重定向模型数据传输
4. 使用 HttpMessageConverter 获取参数与返回数据模型
- 使用 HttpMessageConverter
- 处理 XML/JSON 类型数据

#### 其他功能
1. 静态资源处理
2. 拦截器
3. 异常处理
4. RequestContextHolder 
5. 文件上传
6. 跨域

### 框架结构
1. DispatcherServlet 类简单分析
2. DispatcherServlet 请求流程详解
3. Spring MVC 上下文容器始化
4. MVC 设计

### 数据绑定功能详解
#### 数据绑定流程
#### 数据解析
#### 数据转换
#### 数据格式化
#### 数据校验
#### HTTP 消息转换

### 视图和视图解析器
1. 视图
2. 视图解析器