package com.learning.springmvc.advance.other.interceptor;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * ClassName: LogInterceptor
 * Description: 过滤器
 * Date: 2019/5/24 9:38 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
public class LogInterceptorAdapter extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        System.out.println("LogInterceptorAdapter: " + handler);
        return super.preHandle(request, response, handler);
    }
}