package com.learning.springmvc.advance.file.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.io.IOException;

/**
 * ClassName: WebMvcConfig
 * Description: Spring MVC 配置 继承自{@link WebMvcConfigurerAdapter}。可以理解为 WebApplicationContext 容器的配置
 * <p>
 * Date: 2019/5/13 15:46 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.learning.springmvc.advance", useDefaultFilters = false,
        includeFilters = {@ComponentScan.Filter(classes = Controller.class)})
public class WebMvcConfig extends WebMvcConfigurerAdapter {


    @Override
    public void configureViewResolvers(ViewResolverRegistry registry) {
        registry.jsp("/WEB-INF/view/", ".jsp");
    }

    @Bean
    public CommonsMultipartResolver multipartResolver() throws IOException {
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
        multipartResolver.setDefaultEncoding("UTF-8");
        // 在将上传写入磁盘之前，设置允许的最大大小(以字节为单位)。
        // 上传的文件仍然会被接收超过这个数量，但它们不会存储在内存中。根据Commons FileUpload，默认值是10240。
        multipartResolver.setMaxInMemorySize(10240);
        multipartResolver.setMaxUploadSize(5242880);
        multipartResolver.setMaxUploadSizePerFile(-1);
        // 设置是否在文件或参数访问时延迟解析多部分请求。
        multipartResolver.setResolveLazily(false);
        // 设置是否保留客户端发送的文件名，而不删除CommonsMultipartFile#getOriginalFilename()中的路径信息
        multipartResolver.setPreserveFilename(false);
        // 设置存储上传文件的临时目录。
        // 默认是servlet容器用于web应用程序的临时目录。
        // multipartResolver.setUploadTempDir();
        return multipartResolver;
    }
}
