package com.learning.springmvc.advance.file.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;

/**
 * ClassName: ApplicationConfig
 * Description: Spring 配置。 可以理解为 ApplicationContext 容易的配置
 * Date: 2019/5/13 15:53 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Configuration
@ComponentScan(basePackages = {"com.learning.springmvc.advance"},
        excludeFilters = {@ComponentScan.Filter(classes = Controller.class)})
public class ApplicationConfig {
}
