package com.learning.springmvc.base.controllers.controller;

import com.learning.springmvc.base.controllers.bean.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * ClassName: UserForwardAndRedirectController
 * Description:
 * Date: 2019/8/21 16:00 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Controller
@RequestMapping("user/forwardAndRedirect")
public class UserForwardAndRedirectController {


    /**
     * forward: 一次请求，数据模型会传递
     *
     * @param user
     * @return
     */
    @RequestMapping(value = "/get")
    public String get(User user) {
        // forward: 一次请求，数据模型会传递，直接可以获取
        System.out.println("参数： " + user);
        return "user";
    }

    /**
     * @param user
     * @return
     */
    @RequestMapping(value = "/getName")
    public String getName(User user) {
        System.out.println("参数： " + user);
        return "user";
    }


    /**
     * 地址：http://localhost:8080/user/forwardAndRedirect/forward?name=sam
     * <p>
     * 隐藏的数据模型，会把 user 对象放入数据模型，这时直接就可以从 ModelMap 中获取
     * <p>
     * forward: 一次请求，数据模型会传递
     * <p>
     * 格式：forward: + / + ClassUrl + methodUrl
     *
     * @return
     */
    @RequestMapping(value = "/forward")
    public String forward(User user, ModelMap modelMap) {
        System.out.println("参数： " + modelMap.get("user"));
        return "forward:/user/forwardAndRedirect/get";
    }


    /**
     * 地址： http://localhost:8080/user/forwardAndRedirect/redirect?name=sam
     * <p>
     * 同一个 Controller 中: redirect关键字 + MethodUrl
     *
     * @return
     */
    @RequestMapping(value = "/redirect")
    public String redirect(User user, RedirectAttributes model) {

        System.out.println("参数： " + user);

        // 重定向，两个请求，需要使用 RedirectAttributes#addFlashAttribute 方法
        model.addFlashAttribute("user", user);
        return "redirect:getName";
    }

    /**
     * 地址： http://localhost:8080/user/forwardAndRedirect/redirect2?name=sam
     * <p>
     * 不同的 Controller 中：redirect关键字 +" / " + ClassUrl + MethodUrl
     *
     * @param user
     * @return
     */
    @RequestMapping(value = "/redirect2")
    public String redirect2(User user, RedirectAttributes model) {
        System.out.println("参数： " + user);
        model.addFlashAttribute("user", user);
        return "redirect:/user/forwardAndRedirect/getName";
    }

}
