package com.learning.springmvc.base.controllers.controller;

import com.learning.springmvc.base.controllers.bean.User;
import com.learning.springmvc.base.controllers.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.concurrent.Callable;

/**
 * ClassName: UserRestController
 * Description: Rest 控制
 *
 * RestTemplate
 * AsyncRestTemplate，支持异步无阻塞方式进行访问
 *
 * @author SAM SHO
 * @version V1.0
 */
@RestController
@RequestMapping(value = "user/async")
public class UserAsyncRestController {

    @Autowired
    private UserService userService;

    /**
     * 一个异步的方法
     *
     * @param name
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/call/{name}")
    public Callable<User> call(@PathVariable String name) {
        return new Callable<User>() {
            @Override
            public User call() throws Exception {
                Thread.sleep(2000L);
                return userService.get(name);
            }
        };
    }


}
