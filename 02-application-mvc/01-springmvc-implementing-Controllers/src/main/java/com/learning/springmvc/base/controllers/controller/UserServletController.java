package com.learning.springmvc.base.controllers.controller;

import com.learning.springmvc.base.controllers.bean.User;
import com.learning.springmvc.base.controllers.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * ClassName: UserServletController
 * Description: servlet 控制
 *
 * @author SAM SHO
 * @version V1.0
 */
@Controller
@RequestMapping(value = "user/servlet")
public class UserServletController {

    @Autowired
    private UserService userService;

    /**
     * 注解 @CookieValue 绑定请求中的Cookie值
     * <p>
     * http://localhost:8080/user/servlet/getCookieValue/sam
     */
    @ResponseBody
    @RequestMapping(value = "/getCookieValue/{name}")
    public User getCookieValue(@PathVariable String name,
                               @CookieValue(value = "JSESSIONID", required = false) String sessionId) {
        System.out.println(sessionId);
        return userService.get(name);
    }

    /**
     * 注解 @RequestHeader 绑定请报文头的属性值
     * <p>
     * http://localhost:8080/user/servlet/getRequestHeader/sam
     *
     * @param name
     * @param encoding
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/getRequestHeader/{name}")
    public User getRequestHeader(@PathVariable String name,
                                 @RequestHeader("Accept-Encoding") String encoding) {
        System.out.println(encoding);
        return userService.get(name);
    }


}
