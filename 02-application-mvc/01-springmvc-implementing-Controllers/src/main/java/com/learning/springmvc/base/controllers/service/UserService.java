package com.learning.springmvc.base.controllers.service;


import com.learning.springmvc.base.controllers.bean.User;

import java.util.List;

/**
 * ClassName: UserService
 * Description:
 * Date: 2019/5/13 15:02 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
public interface UserService {

    /**
     * 获取用户
     *
     * @param name
     * @return
     */
    User get(String name);


    /**
     * 列表
     *
     * @return
     */
    List<User> list();
}
