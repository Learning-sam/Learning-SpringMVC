package com.learning.springmvc.base.controllers.bean;


import com.alibaba.fastjson.JSON;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * ClassName: User
 * Description:
 * Date: 2019/8/9 17:27 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Setter
@Getter
public class User {

    private String name;
    private String mobile;
    private Date birthday;
    private Date createDate;

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}

