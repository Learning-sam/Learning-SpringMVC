package com.learning.springmvc.base.controllers.controller;

import com.learning.springmvc.base.controllers.bean.User;
import com.learning.springmvc.base.controllers.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * ClassName: DemoController
 * Description: 一些基础功能
 * <p>
 * springmvc 配置详解
 * https://docs.spring.io/spring/docs/4.3.18.RELEASE/spring-framework-reference/htmlsingle/#mvc
 * Date: 2016/12/22 21:00
 *
 * @author SAM SHO
 * @version V1.0
 */
@Controller
@RequestMapping(value = "user")
public class UserReturnController {

    @Autowired
    private UserService userService;

    /**
     * 使用 ModelAndView 返回
     * <p>
     * http://localhost:8080/user/returnModelAndView?name=sam
     *
     * @param name
     * @return
     */
    @RequestMapping(value = "/returnModelAndView")
    public ModelAndView returnModelAndView(String name) {
        User user = userService.get(name);

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("user", user);
        modelAndView.setViewName("user");
        return modelAndView;
    }

    /**
     * 返回字符串视图，使用 ModelMap 返回数据
     * <p>
     * http://localhost:8080/user/returnString?name=sam
     *
     * @param name
     * @param modelMap
     * @return
     */
    @GetMapping(value = "/returnString")
    public String returnString(String name, ModelMap modelMap) {
        User user = userService.get(name);
        modelMap.addAttribute("user", user);
        return "user";
    }

    /**
     * 使用 Model 返回数据
     * <p>
     * http://localhost:8080/user/returnModel?name=sam
     *
     * @param name
     * @param model
     * @return
     */
    @RequestMapping(value = "/returnModel")
    public String returnModel(String name, Model model) {
        User user = userService.get(name);
        model.addAttribute("user", user);
        return "user";
    }

    /**
     * 使用 Map 返回数据
     * <p>
     * http://localhost:8080/user/returnMap?name=sam
     *
     * @param name
     * @param map
     * @return
     */
    @RequestMapping(value = "/returnMap")
    public String returnMap(String name, Map<String, Object> map) {
        User user = userService.get(name);
        map.put("user", user);
        return "user";
    }

    /**
     * 方法参数直接 @ModelAttribute 绑定返回
     * <p>
     * http://localhost:8080/user/returnModelAttribute?name=sam
     *
     * @param user
     * @return
     */
    @RequestMapping(value = "/returnModelAttribute")
    public String returnModelAttribute(@ModelAttribute User user) {
        user.setName("Eason");
        user = userService.get(user.getName());
        return "user";
    }


    /**
     * 这个Controller类中的任何一个请求处理方法前，spring mvc 都会先执行该方法，并将返回值以user为键添加到模型中
     *
     * @return
     */
    @ModelAttribute("user")
    public User getUser() {
        return userService.get("eason");
    }

    /**
     * 没有任何数据，但是springmvc会先把 @ModelAttribute("user") 模型数据给到这个方法
     * <p>
     * http://localhost:8080/user/returnModelAttributeMethod
     *
     * @return
     */
    @RequestMapping(value = "/returnModelAttributeMethod")
    public String returnModelAttribute(ModelMap modelMap) {
        System.out.println("参数： " + modelMap.get("user"));
        return "user";
    }

}
