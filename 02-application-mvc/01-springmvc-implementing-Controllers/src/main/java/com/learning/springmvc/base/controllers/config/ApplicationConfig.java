package com.learning.springmvc.base.controllers.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;

/**
 * ClassName: ApplicationConfig
 * Description:
 * Date: 2019/5/13 15:53 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Configuration
@ComponentScan(basePackages = {"com.learning.springmvc.base.controllers"},
        excludeFilters = {@ComponentScan.Filter(classes = Controller.class)})
public class ApplicationConfig {
}
