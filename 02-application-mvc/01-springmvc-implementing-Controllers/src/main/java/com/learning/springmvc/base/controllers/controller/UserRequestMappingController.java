package com.learning.springmvc.base.controllers.controller;

import com.learning.springmvc.base.controllers.bean.User;
import com.learning.springmvc.base.controllers.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * ClassName: DemoController
 * Description: 一些基础功能
 * <p>
 * springmvc 配置详解
 * https://docs.spring.io/spring/docs/4.3.18.RELEASE/spring-framework-reference/htmlsingle/#mvc
 * Date: 2016/12/22 21:00
 *
 * @author SAM SHO
 * @version V1.0
 */
@Controller
@RequestMapping(value = "user")
public class UserRequestMappingController {

    @Autowired
    private UserService userService;


    /**
     * 注解 RequestMapping 参数
     * <p>
     * http://localhost:8080/user/get?name=sam
     * <p>
     * 约定需要参数name，没有回报错
     *
     * @param name
     * @param modelMap
     * @return
     */
    @RequestMapping(value = "/get", method = RequestMethod.GET,
            headers = {"content-type=text/*"},
            params = {"name"},
            consumes = {"text/plain", "application/*"},
            produces = {"text/plain", "application/*"})
    public String get(@RequestParam("name") String name, ModelMap modelMap) {
        User user = userService.get(name);
        modelMap.addAttribute("user", user);
        return "user";
    }

    /**
     * 组合注解 GetMapping
     * <p>
     * http://localhost:8080/user/getMapping?name=sam
     *
     * @param name
     * @param modelMap
     * @return
     */
    @GetMapping(value = "/getMapping",
            headers = {"content-type=text/*"},
            params = {"name"},
            consumes = {"text/plain", "application/*"},
            produces = {"text/plain", "application/*"})
    public String getMapping(String name, ModelMap modelMap) {
        User user = userService.get(name);
        modelMap.addAttribute("user", user);
        return "user";
    }

    /**
     * 组合注解 PostMapping
     *
     * @param user
     * @param modelMap
     * @return
     */
    @PostMapping(value = "/postMapping")
    public String postMapping(User user, ModelMap modelMap) {
        user = userService.get(user.getName());
        modelMap.addAttribute("user", user);
        return "user";
    }


}
