package com.learning.springmvc.base.controllers.controller;

import com.learning.springmvc.base.controllers.bean.User;
import com.learning.springmvc.base.controllers.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.format.datetime.DateFormatter;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * ClassName: UserRestController
 * Description: Rest 控制
 * <p>
 * 注解 @ResponseBody、@RequestBody 等，需要 @EnableWebMvc 支持
 * <p>
 * 注解 @RestController 组合了 @Controller、@ResponseBody
 *
 * @author SAM SHO
 * @version V1.0
 */
@Controller
//@RestController
@RequestMapping(value = "user/rest")
public class UserRestController {

    @Autowired
    private UserService userService;


    /**
     * 将请求报文体转换为字符串绑定到  RequestBody 入参中
     * StringHttpMessageConverter
     *
     * @param name
     * @return
     */
    @RequestMapping(value = "/RequestBody")
    public String RequestBody(@RequestBody String name) {
        System.out.println(name);
        return "user";
    }

    /**
     * 将数据传输到响应流中
     * ByteArrayHttpMessageConverter
     *
     * @param name
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/responseBody/{name}")
    public byte[] responseBodyByte(@PathVariable("name") String name) {
        System.out.println(name);
        return name.getBytes();
    }


    @RequestMapping(value = "/requestEntity")
    public String RequestBody(RequestEntity<String> entity) {
        System.out.println(entity.getBody());
        return "user";
    }

    @RequestMapping(value = "/responseEntity")
    public ResponseEntity<byte[]> responseEntity() {
        return new ResponseEntity("Sam".getBytes(), HttpStatus.OK);
    }


    /*************************** 处理XML/JSON ***********************8*/


    /**
     * 使用 ResponseBody 返回数据
     * <p>
     * http://localhost:8080/user/rest/responseBody?name=sam
     *
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/json/responseBody")
    public User responseBody(String name) {
        return userService.get(name);
    }

    /**
     * 使用 ResponseBody 返回数据
     * <p>
     * http://localhost:8080/user/rest/responseBodyList
     *
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/json/responseBodyList")
    public List<User> responseBody() {
        return userService.list();
    }

    /**
     * 使用 RequestBody 接收数据
     * <p>
     * Content-Type application/json
     * POST http://localhost:8080/user/rest/requestBody
     *
     * @param user
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/json/requestBody")
    public User requestBody(@RequestBody User user) {
        return user;
    }

    /**
     * 使用 ResponseEntity 返回
     * <p>
     * http://localhost:8080/user/rest/responseEntity?name=sam
     *
     * @param name
     * @return
     */
    @RequestMapping(value = "/json/responseEntity")
    public ResponseEntity<User> responseEntity(String name) {
        User user = userService.get(name);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    /**
     * 使用 RequestEntity 接收数据
     *
     * @param entity
     * @return
     */
    @RequestMapping(value = "/json/requestEntity")
    public User requestEntity(RequestEntity<User> entity) {
        return entity.getBody();
    }


}
