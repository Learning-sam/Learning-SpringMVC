package com.learning.springmvc.base.controllers.controller;

import com.learning.springmvc.base.controllers.bean.User;
import org.junit.Test;
import org.springframework.http.ResponseEntity;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;
import org.springframework.web.client.AsyncRestTemplate;

import static org.junit.Assert.*;

/**
 * ClassName: UserAsyncRestControllerTest
 * Description:
 * Date: 2019/5/14 11:32 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
public class UserAsyncRestControllerTest {

    @Test
    public void call() throws InterruptedException {

        AsyncRestTemplate template = new AsyncRestTemplate();

        ListenableFuture<ResponseEntity<User>> future = template.getForEntity("http://localhost:8080/user/async/call/sam", User.class);

        future.addCallback(new ListenableFutureCallback<ResponseEntity<User>>() {


            @Override
            public void onSuccess(ResponseEntity<User> result) {
                System.out.println("获取数据返回： " + result.getBody());
            }

            @Override
            public void onFailure(Throwable ex) {

            }
        });

        Thread.sleep(5000L);
    }
}