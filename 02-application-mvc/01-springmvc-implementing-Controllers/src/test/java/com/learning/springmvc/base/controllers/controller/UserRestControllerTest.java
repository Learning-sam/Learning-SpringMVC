package com.learning.springmvc.base.controllers.controller;

import com.alibaba.fastjson.JSON;
import com.learning.springmvc.base.controllers.bean.User;
import com.learning.springmvc.base.controllers.config.ApplicationConfig;
import com.learning.springmvc.base.controllers.config.WebMvcConfig;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Date;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * ClassName: UserRestControllerTest
 * Description:
 * Date: 2019/5/14 8:50 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {ApplicationConfig.class, WebMvcConfig.class})
public class UserRestControllerTest {


    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext wac;

    @Before
    public void setUp() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }


    @Test
    public void responseBody() throws Exception {
        MvcResult result = mockMvc.perform(get("/rest/first"))
                .andExpect(status().isOk())
                .andDo(print())
                .andReturn();
    }

    @Test
    public void requestBody() throws Exception {
        User user = new User();
        user.setName("Sam");
        user.setMobile("123456");
        user.setBirthday(new Date());
        String jsonString = JSON.toJSONString(user);

        MvcResult result = mockMvc.perform(
                post("/rest/second")
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
                        .content(jsonString))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.name").value("Sam"))
                .andDo(print())
                .andReturn();
    }

    @Test
    public void responseEntity() {
    }

    @Test
    public void requestEntity() {
    }
}