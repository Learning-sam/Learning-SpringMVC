package com.learning.springmvc.advance.exception.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * ClassName: UserController
 * Description:
 * Date: 2019/5/23 16:27 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@RestController
@RequestMapping("/user")
public class UserController {


    /**
     * http://localhost:8080/static.js 访问静态文件
     * <p>
     * http://localhost:8080/user/interceptor 过滤器生效
     *
     * @return
     */
    @GetMapping("/interceptor")
    public String interceptor() {
        return "interceptor";
    }
}
