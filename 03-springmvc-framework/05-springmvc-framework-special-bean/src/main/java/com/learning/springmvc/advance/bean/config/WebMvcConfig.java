package com.learning.springmvc.advance.bean.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.validation.MessageCodesResolver;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.HandlerMethodReturnValueHandler;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.List;

/**
 * ClassName: WebMvcConfig
 * Description: Spring MVC 配置 继承自{@link WebMvcConfigurerAdapter}。可以理解为 WebApplicationContext 容器的配置
 * <p>
 * <p>
 * 默认配置： DispatcherServlet.properties 这个文件中。实现在类{@link WebMvcConfigurationSupport}
 * <p>
 * <p>
 * Date: 2019/5/13 15:46 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.learning.springmvc.advance", useDefaultFilters = false,
        includeFilters = {@ComponentScan.Filter(classes = Controller.class)})
public class WebMvcConfig extends WebMvcConfigurerAdapter {


    /**
     * DispatcherServlet.properties 这个文件中的配置
     *
     *
     * HandlerMapping：org.springframework.web.servlet.HandlerMapping
     *  org.springframework.web.servlet.handler.BeanNameUrlHandlerMapping,
     * 	org.springframework.web.servlet.mvc.annotation.DefaultAnnotationHandlerMapping
     *
     * HandlerAdapter：org.springframework.web.servlet.HandlerAdapter
     *  org.springframework.web.servlet.mvc.HttpRequestHandlerAdapter,
     * 	org.springframework.web.servlet.mvc.SimpleControllerHandlerAdapter,
     * 	org.springframework.web.servlet.mvc.annotation.AnnotationMethodHandlerAdapter
     *
     * HandlerExceptionResolver：org.springframework.web.servlet.HandlerExceptionResolver
     *  org.springframework.web.servlet.mvc.annotation.AnnotationMethodHandlerExceptionResolver,
     * 	org.springframework.web.servlet.mvc.annotation.ResponseStatusExceptionResolver,
     * 	org.springframework.web.servlet.mvc.support.DefaultHandlerExceptionResolver
     *
     * ViewResolver：org.springframework.web.servlet.ViewResolver
     *  org.springframework.web.servlet.view.InternalResourceViewResolver
     *
     * LocaleResolver & LocaleContextResolver：org.springframework.web.servlet.LocaleResolver
     *  org.springframework.web.servlet.i18n.AcceptHeaderLocaleResolver
     *
     * ThemeResolver：org.springframework.web.servlet.ThemeResolver
     *  org.springframework.web.servlet.theme.FixedThemeResolver
     *
     * MultipartResolver
     *HandlerMethodArgumentResolver
     * FlashMapManager：org.springframework.web.servlet.FlashMapManager
     *  org.springframework.web.servlet.support.SessionFlashMapManager
     */


    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {

    }

    @Override
    public void addReturnValueHandlers(List<HandlerMethodReturnValueHandler> returnValueHandlers) {

    }
}
