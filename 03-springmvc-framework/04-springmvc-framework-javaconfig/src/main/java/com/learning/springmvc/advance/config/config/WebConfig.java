package com.learning.springmvc.advance.config.config;

import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

/**
 * ClassName: WebConfig
 * Description:
 * Date: 2019/5/15 11:02 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
public class WebConfig extends WebMvcConfigurationSupport {
}
