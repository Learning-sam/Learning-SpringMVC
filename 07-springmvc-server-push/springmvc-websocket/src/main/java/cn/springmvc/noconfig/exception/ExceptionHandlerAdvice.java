package cn.springmvc.noconfig.exception;

import cn.springmvc.noconfig.entity.ErrorInfo;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * ClassName: ExceptionHandlerAdvice
 * Desc: 统一异常处理，返回JSON
 * Date： 2017/5/3
 * Created：shaom
 */
@ControllerAdvice
public class ExceptionHandlerAdvice {

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public ErrorInfo<String> jsonErrorHandler(HttpServletRequest req, Exception e)  {
        ErrorInfo<String> r = new ErrorInfo<>();
        r.setMessage(e.getMessage());
        r.setCode("500");
        r.setData("Some Data");
        r.setUrl(req.getRequestURL().toString());
        return r;
    }
}
