package cn.springmvc.noconfig.converter;

import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;

import java.io.IOException;
import java.nio.charset.Charset;

/**
 * ClassName: MyMessageConverter
 * Desc:  自定义的MessageConverter。需要注册到配置中
 * 可以实现 HttpMessageConverter 接口，或者继承 AbstractHttpMessageConverter
 * 可以参考 StringHttpMessageConverter 和 MappingJackson2HttpMessageConverter
 * Date： 2017/5/4
 * Created：shaom
 */
public class MyMessageConverter extends AbstractHttpMessageConverter {

    /**
     * 定义处理的MediaType, application/x-type
     */
    public MyMessageConverter() {
        super(new MediaType("application", "x-type", Charset.forName("UTF-8")));
    }

    /**
     * 支持处理的类型,只处理改类型，覆盖canRead和canWrite方法一样的效果
     */
    @Override
    protected boolean supports(Class clazz) {
//        return String.class.equals(clazz);
        return true;
    }

    /**
     * 处理请求的数据，可以由 inputMessage 数据转成我们想要的数据
     *
     * @param clazz
     * @param inputMessage
     * @return
     * @throws IOException
     * @throws HttpMessageNotReadableException
     */
    @Override
    protected Object readInternal(Class clazz, HttpInputMessage inputMessage) throws IOException, HttpMessageNotReadableException {
        return null;
    }

    /**
     * 如何处理输出数据到response
     *
     * @param o
     * @param outputMessage
     * @throws IOException
     * @throws HttpMessageNotWritableException
     */
    @Override
    protected void writeInternal(Object o, HttpOutputMessage outputMessage) throws IOException, HttpMessageNotWritableException {

    }
}
