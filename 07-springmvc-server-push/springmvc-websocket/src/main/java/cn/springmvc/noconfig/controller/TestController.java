package cn.springmvc.noconfig.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * ClassName: TestController
 * Desc:
 * Date： 2017/5/3
 * Created：shaom
 */
@Controller
public class TestController {

    /**
     * 配置 addViewControllers 后，这种简单跳转可以省略
     *
     * @return
     */
    @RequestMapping("test2")
    public String test() {
        return "test";
    }

    @RequestMapping("trim")
    public ResponseEntity<String> trim(String name) {
        System.out.println(name);
        return new ResponseEntity<>(name, HttpStatus.OK);
    }
}
