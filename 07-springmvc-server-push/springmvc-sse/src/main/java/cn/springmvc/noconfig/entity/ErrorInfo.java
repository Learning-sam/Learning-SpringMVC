package cn.springmvc.noconfig.entity;

/**
 * ClassName: ErrorInfo
 * Desc:
 * Date： 2017/5/3
 * Created：shaom
 */
public class ErrorInfo<T> {

    private String message;
    private String code;
    private String data;
    private String url;

    public String getMessage() {
        return message;
    }

    public String getCode() {
        return code;
    }

    public String getData() {
        return data;
    }

    public String getUrl() {
        return url;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setData(String data) {
        this.data = data;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
