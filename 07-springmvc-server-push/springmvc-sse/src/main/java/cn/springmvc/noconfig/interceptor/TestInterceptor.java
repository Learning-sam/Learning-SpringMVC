package cn.springmvc.noconfig.interceptor;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * ClassName: TestInterceptor
 * Desc:  implements HandlerInterceptor Or  extends HandlerInterceptorAdapter
 * Date： 2017/5/3
 * Created：shaom
 */
public class TestInterceptor extends HandlerInterceptorAdapter {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        System.out.println(" ############ TestInterceptor ##################");
        return true;
    }
}
