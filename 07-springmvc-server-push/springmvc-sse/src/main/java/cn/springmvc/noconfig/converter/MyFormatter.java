package cn.springmvc.noconfig.converter;

import org.springframework.format.Formatter;

import java.text.ParseException;
import java.util.Locale;

/**
 * ClassName: MyFormatter
 * Desc: 自定义的格式化器，可以参考 DateFormatter 和 AbstractNumberFormatter
 * Date： 2017/5/4
 * Created：shaom
 */
public class MyFormatter implements Formatter<Object> {

    @Override
    public Object parse(String text, Locale locale) throws ParseException {
        return text;
    }

    @Override
    public String print(Object object, Locale locale) {
        return object.toString();
    }
}
