<%--
  Created by IntelliJ IDEA.
  User: home
  Date: 2016/12/22
  Time: 20:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>

    <script type="application/javascript" src="${ctx.contextPath}/resource/js/jquery-2.1.1.min.js"></script>
</head>
<body>
HELLO!! ${name}


<script>

    $(function () {

//        sse();
        service();

    });

    /**
     * servlet3.0+ 异步调用
     */
    function service() {
        $.get('${ctx.contextPath}/service', function (data) {
            alert(data);
            console.info(data);
            service();// 递归调用
        })
    }

    /**
     * sse技术前端
     */
    function sse() {
        if (!!window.EventSource) {

            var eventSource = new EventSource('sse');

            eventSource.addEventListener('message', function (e) {
                console.info(e.data);
            });

            eventSource.addEventListener('open', function (e) {
                console.info("open")
            }, false);

            eventSource.addEventListener('error', function (e) {
                console.info("error");
            }, false);

        } else {
            console.info("你的浏览器不支持SSE");
        }
    }


</script>
</body>
</html>
