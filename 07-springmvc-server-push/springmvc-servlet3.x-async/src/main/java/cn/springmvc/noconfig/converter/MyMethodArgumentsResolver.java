package cn.springmvc.noconfig.converter;

import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import java.util.Iterator;

/**
 * ClassName: MyMethodArgumentsResolver
 * Desc:  处理自定义的参数类型,利用在参数上加注解
 * Date： 2017/5/4
 * Created：shaom
 */
public class MyMethodArgumentsResolver implements HandlerMethodArgumentResolver {

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.hasParameterAnnotation(ApiQuery.class);
    }

    @Override
    public Object resolveArgument(MethodParameter parameter,
                                  ModelAndViewContainer mavContainer,
                                  NativeWebRequest webRequest,
                                  WebDataBinderFactory binderFactory) throws Exception {

        Class<?> targetType = parameter.getParameterType();
        String param = parameter.getParameterAnnotation(ApiQuery.class).value();
        Iterator<String> it = webRequest.getParameterNames();
        return null;
    }
}
