package cn.springmvc.noconfig.converter;

import com.google.common.base.Strings;
import org.springframework.core.convert.converter.Converter;

/**
 * ClassName: MyConverter
 * Desc:  自定义的 Converter ，用于接收到的数据去空格，有参数才会进来
 * Date： 2017/5/4
 * Created：shaom
 */
public class MyConverter implements Converter<String, String> {

    @Override
    public String convert(String source) {
        if (!Strings.isNullOrEmpty(source)) {
            source = source.trim();//去掉空格
        }
        return source;
    }
}
