package cn.springmvc.noconfig.converter;

import java.lang.annotation.*;

@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ApiQuery {
    String value() default "";
}