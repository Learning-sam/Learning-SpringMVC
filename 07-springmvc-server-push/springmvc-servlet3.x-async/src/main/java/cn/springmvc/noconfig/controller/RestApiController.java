package cn.springmvc.noconfig.controller;

import cn.springmvc.noconfig.entity.User;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * ClassName: RestController
 * Desc:
 * Date： 2017/5/3
 * Created：shaom
 */
@RestController
public class RestApiController {

    @RequestMapping("rest")
    public User test() {
        User user = new User();
        user.setName("sam");
        user.setPwd("123123");
        return user;
    }
}
