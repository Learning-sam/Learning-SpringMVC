package com.learning.springmvc.base.javaconfig.service;

import com.learning.springmvc.base.javaconfig.bean.User;

/**
 * ClassName: UserService
 * Description:
 * Date: 2019/5/13 15:02 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
public interface UserService {

    /**
     * 获取用户
     *
     * @param name
     * @return
     */
    User get(String name);
}
