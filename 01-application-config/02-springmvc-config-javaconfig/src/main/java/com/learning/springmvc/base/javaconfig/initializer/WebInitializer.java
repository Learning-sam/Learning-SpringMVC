package com.learning.springmvc.base.javaconfig.initializer;

import org.springframework.util.ObjectUtils;
import org.springframework.web.SpringServletContainerInitializer;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletContainerInitializer;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

/**
 * ClassName: WebInitializer
 * Description:  初始化类，代替 web.xml配置
 * <p>
 * 需要 Servlet3.0 支持 {@link ServletContainerInitializer} and {@link SpringServletContainerInitializer}
 * <p>
 * Date: 2019/5/13 15:46 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Deprecated
public class WebInitializer implements WebApplicationInitializer {
    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {

        // Spring 配置。见 AbstractDispatcherServletInitializer#onStartup(servletContext)
//        AnnotationConfigWebApplicationContext rootAppContext = new AnnotationConfigWebApplicationContext();
////        rootAppContext.register(ApplicationConfig.class);
////        ContextLoaderListener listener = new ContextLoaderListener(rootAppContext);
////        servletContext.addListener(listener);
////
////
////        // Spring Mvc 配置。见 AbstractDispatcherServletInitializer#registerDispatcherServlet(servletContext)
////        AnnotationConfigWebApplicationContext servletAppContext = new AnnotationConfigWebApplicationContext();
////        servletAppContext.register(WebMvcConfig.class);
////
////
////        DispatcherServlet dispatcherServlet = new DispatcherServlet(servletAppContext);
////        ServletRegistration.Dynamic registration = servletContext.addServlet("dispatcher", dispatcherServlet);
////        registration.setLoadOnStartup(1);
////        registration.addMapping("/");
////        registration.setAsyncSupported(true);
    }
}
