package com.learning.springmvc.base.xml.bean;

import com.alibaba.fastjson.JSON;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * ClassName: User
 * Description:
 * Date: 2016/12/22 21:02
 *
 * @author SAM SHO
 * @version V1.0
 */
@Setter
@Getter
public class User {

    private String name;
    private String mobile;
    private Date birthday;
    private Date createDate;

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}
