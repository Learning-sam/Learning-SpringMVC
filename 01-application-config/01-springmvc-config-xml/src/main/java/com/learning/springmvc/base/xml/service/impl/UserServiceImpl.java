package com.learning.springmvc.base.xml.service.impl;

import com.google.common.collect.Maps;
import com.learning.springmvc.base.xml.bean.User;
import com.learning.springmvc.base.xml.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Map;

/**
 * ClassName: UserServiceImpl
 * Description:
 * Date: 2019/5/13 15:02 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Service
public class UserServiceImpl implements UserService {

    private Map<String, User> users = Maps.newHashMap();


    @Override
    public User get(String name) {
        return users.get(name);
    }


    @PostConstruct
    public void init() {
        User user = new User();
        user.setName("sam");
        user.setMobile("123456");
        users.put(user.getName(), user);

        user = new User();
        user.setName("eason");
        user.setMobile("987654");
        users.put(user.getName(), user);
    }
}
