package com.learning.springmvc.base.xml.controller;

import com.learning.springmvc.base.xml.bean.User;
import com.learning.springmvc.base.xml.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * ClassName: DemoController
 * Description: 一些基础功能
 * <p>
 * springmvc 配置详解
 * https://docs.spring.io/spring/docs/4.3.18.RELEASE/spring-framework-reference/htmlsingle/#mvc
 * Date: 2016/12/22 21:00
 *
 * @author SAM SHO
 * @version V1.0
 */
@Controller
@RequestMapping(value = "user")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/get")
    public String get(String name, ModelMap modelMap) {
        User user = userService.get(name);
        modelMap.addAttribute("user", user);
        return "base";
    }


}
