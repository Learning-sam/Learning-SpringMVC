## xml 配置的发起点：

```
org.springframework.web.servlet.DispatcherServlet.DispatcherServlet()

public DispatcherServlet() {
super();
setDispatchOptionsRequest(true);
}
```

2. `super();` 方法，调用父类
```
org.springframework.web.servlet.FrameworkServlet.FrameworkServlet()

public FrameworkServlet() {
}

```

3. `setDispatchOptionsRequest(true);`


## FrameworkServlet 类是一个关键
1. 继承自`HttpServletBean` ，实现`ApplicationContextAware`

```
class FrameworkServlet extends HttpServletBean implements ApplicationContextAware
```

2. `HttpServletBean` 继承`javax.servlet.http.HttpServlet#init()`方法。
- 这个方法中有一个关键的方法`initServletBean();`
- FrameworkServlet 重写了 `initServletBean`方法。创建 Creates this servlet's WebApplicationContext.

```
# 实现初始化 servlet's WebApplicationContext.
this.webApplicationContext = initWebApplicationContext();

# DispatcherServlet 重写
initFrameworkServlet();
```

3.从 ServletContext 中通过下面的key获取 `WebApplicationContext` 容器

```
org.springframework.web.context.WebApplicationContext.ROOT
```
