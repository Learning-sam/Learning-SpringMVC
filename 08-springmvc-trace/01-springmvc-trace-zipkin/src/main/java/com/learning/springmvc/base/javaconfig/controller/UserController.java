package com.learning.springmvc.base.javaconfig.controller;

import com.learning.springmvc.base.javaconfig.bean.User;
import com.learning.springmvc.base.javaconfig.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * ClassName: DemoController
 * Description: 一些基础功能
 * <p>
 * springmvc 配置详解
 * https://docs.spring.io/spring/docs/4.3.18.RELEASE/spring-framework-reference/htmlsingle/#mvc
 * Date: 2016/12/22 21:00
 *
 * @author SAM SHO
 * @version V1.0
 */
@RestController
@RequestMapping(value = "user")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping(value = "/get/{name}")
    public User get(@PathVariable("name") String name) {
        return userService.get(name);
    }


}
