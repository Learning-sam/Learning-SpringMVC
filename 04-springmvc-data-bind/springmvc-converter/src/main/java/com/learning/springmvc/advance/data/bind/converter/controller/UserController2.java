package com.learning.springmvc.advance.data.bind.converter.controller;

import com.learning.springmvc.advance.data.bind.converter.bean.User;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.format.datetime.DateFormatter;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * ClassName: UserController
 * Description:
 * Date: 2019/5/21 16:23 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@RestController
@RequestMapping("user2")
public class UserController2 {

    /**
     * 在 UserController 中添加的 DateFormatter 在 UserController2 中无效。
     * 全局性的，需要使用`@ControllerAdvice`或者 `WebBindingInitializer`接口
     * @param date
     * @return
     */
    @GetMapping("date")
    public Date get(Date date) {
        return date;
    }


    /**
     * http://localhost:8080/user2/get?name=sam&mobile=1234
     * <p>
     * <p>配置参数转换器
     * http://localhost:8080/user/get?name=sam&mobile=1234&birthday=2017-02-27 会报错（Date 类型转换）
     * <p>
     * 自定义参数转换器：
     * 使用 StringToUserConverter 后可以这样访问 http://localhost:8080/user/get?user=sam:1234
     * 使用 StringToUserConverter 后 http://localhost:8080/user/get?user=sam:1234:2017-02-27
     *
     * @param user
     * @return
     */
    @GetMapping("get")
    public User get(User user) {
        return user;
    }

}
