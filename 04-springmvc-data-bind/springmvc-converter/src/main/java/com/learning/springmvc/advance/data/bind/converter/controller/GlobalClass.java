package com.learning.springmvc.advance.data.bind.converter.controller;

import org.springframework.format.datetime.DateFormatter;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.InitBinder;

/**
 * ClassName: GlobalClass
 * Description:
 * Date: 2019/12/1 20:27 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@ControllerAdvice
public class GlobalClass {

    /**
     * 作用域：默认 @ControllerAdvice 注解的作用域的 Controller 有效
     * 在 RequestMappingHandlerAdapter#initControllerAdviceCache() 方法中加载装配 @ControllerAdvice 注解的   @InitBinder 方法。
     * @param binder
     */
    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.addCustomFormatter(new DateFormatter("yyyy-MM-dd"));
    }
}
