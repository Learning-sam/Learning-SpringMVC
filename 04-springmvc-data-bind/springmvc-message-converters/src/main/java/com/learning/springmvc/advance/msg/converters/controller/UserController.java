package com.learning.springmvc.advance.msg.converters.controller;

import com.learning.springmvc.advance.msg.converters.bean.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * ClassName: UserController
 * Description:
 * Date: 2019/5/23 16:27 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@RestController
@RequestMapping("/user")
public class UserController {


    /**
     * <p>
     * http://localhost:8080/user/get?name=sam
     *
     * @return
     */
    @GetMapping("/get")
    public String get(User user) {
        return user.getName();
    }


    /**
     * http://localhost:8080/user/post
     *
     * @param user
     * @return
     */
    @PostMapping("/post")
    public User post(@RequestBody User user) {
        return user;
    }
}
