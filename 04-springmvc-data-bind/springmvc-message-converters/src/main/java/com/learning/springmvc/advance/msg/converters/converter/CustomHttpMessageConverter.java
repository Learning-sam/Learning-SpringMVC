package com.learning.springmvc.advance.msg.converters.converter;

import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.converter.AbstractGenericHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;

import java.io.IOException;
import java.lang.reflect.Type;

/**
 * ClassName: CustomHttpMessageConverter
 * Description: 自定义 HttpMessageConverter。
 * <p>
 * 基本不需要自定义，日常需要使用的都已经有了。
 * <p>
 * Date: 2019/5/20 15:21 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
public class CustomHttpMessageConverter extends AbstractGenericHttpMessageConverter<Object> {
    @Override
    protected void writeInternal(Object o, Type type, HttpOutputMessage outputMessage) throws IOException, HttpMessageNotWritableException {
        System.out.println("writeInternal");
    }

    @Override
    protected Object readInternal(Class clazz, HttpInputMessage inputMessage) throws IOException, HttpMessageNotReadableException {

        System.out.println("readInternal");
        return null;
    }

    @Override
    public Object read(Type type, Class contextClass, HttpInputMessage inputMessage) throws IOException, HttpMessageNotReadableException {
        System.out.println("read");
        return null;
    }
}
