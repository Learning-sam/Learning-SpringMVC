package com.learning.springmvc.advance.msg.converters.config;

import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import com.learning.springmvc.advance.msg.converters.converter.CustomHttpMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.converter.xml.MappingJackson2XmlHttpMessageConverter;
import org.springframework.stereotype.Controller;
import org.springframework.validation.MessageCodesResolver;
import org.springframework.validation.Validator;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.HandlerMethodReturnValueHandler;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.config.annotation.AsyncSupportConfigurer;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * ClassName: WebMvcConfig
 * Description: Spring MVC 配置 继承自{@link WebMvcConfigurerAdapter}。可以理解为 WebApplicationContext 容器的配置
 * <p>
 * Date: 2019/5/13 15:46 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "com.learning.springmvc.advance.msg.converters", useDefaultFilters = false,
        includeFilters = {@ComponentScan.Filter(classes = Controller.class)})
public class WebMvcConfig extends WebMvcConfigurerAdapter {





    /*
    * 	protected final List<HttpMessageConverter<?>> getMessageConverters() {
		if (this.messageConverters == null) {
			this.messageConverters = new ArrayList<HttpMessageConverter<?>>();

			// 重写这个方法后，默认方法就不会调用了
			configureMessageConverters(this.messageConverters);
			if (this.messageConverters.isEmpty()) {
			    // 添加默认
				addDefaultHttpMessageConverters(this.messageConverters);
			}
			// 追加上去
			extendMessageConverters(this.messageConverters);
		}
		return this.messageConverters;
	}
    * */


    /**
     * HttpMessageConverter 配置
     * <p>
     * configureMessageConverters: 会覆盖spring默认的 HttpMessageConverter
     *
     * @param converters
     */
    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder()
                .indentOutput(true)
                .dateFormat(new SimpleDateFormat("yyyy-MM-dd"))
                .modulesToInstall(new ParameterNamesModule());
        converters.add(new MappingJackson2HttpMessageConverter(builder.build()));
    }

    /**
     * HttpMessageConverter 配置
     * <p>
     * extendMessageConverters: 仅仅添加一个自定义的HttpMessageConverter，不会覆盖
     *
     * @param converters
     */
    @Override
    public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
        converters.add(customHttpMessageConverter());
    }

    @Bean
    public HttpMessageConverter customHttpMessageConverter() {
        return new CustomHttpMessageConverter();
    }


}
