package com.learning.springmvc.advance.formatter.initializer;

import com.learning.springmvc.advance.formatter.config.ApplicationConfig;
import com.learning.springmvc.advance.formatter.config.WebMvcConfig;
import org.springframework.web.SpringServletContainerInitializer;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import javax.servlet.ServletContainerInitializer;

/**
 * ClassName: WebMvcInitializer
 * Description: 初始化类，代替 web.xml配置,可以处理原生的处理
 * <p>
 * 需要 Servlet3.0 支持，见 {@link ServletContainerInitializer} 和 {@link SpringServletContainerInitializer}、{@link WebApplicationInitializer}
 * <p>
 * <p>
 * DispatcherServlet 内置默认配置的Bean组件：org/springframework/web/servlet/DispatcherServlet.properties
 * <p>
 * Date: 2019/5/13 15:46 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
public class WebMvcInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{ApplicationConfig.class};
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[]{WebMvcConfig.class};
    }

    @Override
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }

}
