package com.learning.springmvc.advance.formatter.format;

import org.springframework.format.Formatter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * ClassName: CusDateFormatter
 * Description: 简单自定义的 DateFormatter
 * Date: 2019/5/22 13:13 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
public class CusDateFormatter implements Formatter<Date> {

    @Override
    public Date parse(String text, Locale locale) throws ParseException {
        return getSimpleDateFormat().parse(text);
    }

    @Override
    public String print(Date date, Locale locale) {
        return getSimpleDateFormat().format(date);
    }


    private SimpleDateFormat getSimpleDateFormat() {
        return new SimpleDateFormat("yyyy-MM-hh");
    }
}
