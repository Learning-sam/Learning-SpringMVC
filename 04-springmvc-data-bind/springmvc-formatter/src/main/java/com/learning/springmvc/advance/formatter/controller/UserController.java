package com.learning.springmvc.advance.formatter.controller;

import com.learning.springmvc.advance.formatter.bean.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * ClassName: UserController
 * Description:
 * Date: 2019/5/21 16:23 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@RestController
@RequestMapping("user")
public class UserController {

    /**
     * 使用 @DateTimeFormat 注解后直接可以
     * http://localhost:8080/user/get?name=sam&mobile=1234&birthday=2017-02-27 直接实现类型格式化
     *
     * @param user
     * @return
     */
    @GetMapping("get")
    public User get(User user) {
        return user;
    }


}
