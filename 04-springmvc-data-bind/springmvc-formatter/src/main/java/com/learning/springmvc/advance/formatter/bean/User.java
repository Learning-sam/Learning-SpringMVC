package com.learning.springmvc.advance.formatter.bean;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * ClassName: User
 * Description:
 * Date: 2016/12/22 21:02
 *
 * @author SAM SHO
 * @version V1.0
 */
@Setter
@Getter
public class User {

    private String name;
    private String mobile;

    /**
     * 数据格式化，注解使用
     */
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE, pattern = "yyyy-MM-hh")
    @JsonFormat(pattern = "yyyy-MM-hh", timezone = "UTC-8")
    private Date birthday;
    private Date createDate;

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}
