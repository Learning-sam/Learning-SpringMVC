package com.learning.springmvc.advance.valid.controller;

import com.learning.springmvc.advance.valid.bean.Address;
import com.learning.springmvc.advance.valid.bean.User;
import com.learning.springmvc.advance.valid.group.Group;
import com.learning.springmvc.advance.valid.group.Insert;
import com.learning.springmvc.advance.valid.group.Select;
import com.learning.springmvc.advance.valid.validator.UserLoginValidator;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * ClassName: UserController
 * Description:
 * Date: 2019/5/21 16:23 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@RestController
@RequestMapping("address")
public class AddressController {

    /**
     * http://localhost:8080/address/validated
     *
     * @param address
     * @return
     */
    @GetMapping("validated")
    public Address validated(@Validated Address address) {
        return address;
    }

    /**
     * http://localhost:8080/address/user
     * @param user
     * @return
     */
    @GetMapping("user")
    public User user(@Validated User user) {
        return user;
    }

}