package com.learning.springmvc.advance.valid.bean;

import com.alibaba.fastjson.JSON;
import com.learning.springmvc.advance.valid.group.Insert;
import com.learning.springmvc.advance.valid.group.Select;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * ClassName: User
 * Description:
 * Date: 2016/12/22 21:02
 * <p>
 * 校验规则：
 * - @UserConstraint name 必须是eason
 * - name 属性不能为空
 * - mobile 在 Insert 分组时不能为空
 * - birthday 在 Select 分组时不能为空
 *
 * @author SAM SHO
 * @version V1.0
 */
@Setter
@Getter
public class Address {

    @NotBlank
    @Length(min = 6)
    private String name;

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}
