package com.learning.springmvc.advance.valid.validator;

import com.learning.springmvc.advance.valid.bean.User;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;

/**
 * ClassName: UserValidator
 * Description: 自定义校验注解校验实现
 * Date: 2019/5/22 13:57 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
public class UserValidator implements ConstraintValidator<UserConstraint, User> {

    private static final String DEFAULT_NAME = "eason";


    @Override
    public void initialize(UserConstraint constraintAnnotation) {

    }

    @Override
    public boolean isValid(User user, ConstraintValidatorContext context) {
        if (Objects.equals(DEFAULT_NAME, user.getName())) {
            return true;
        }
        return false;
    }
}