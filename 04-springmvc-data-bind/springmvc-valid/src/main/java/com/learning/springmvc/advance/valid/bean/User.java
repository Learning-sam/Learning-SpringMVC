package com.learning.springmvc.advance.valid.bean;

import com.alibaba.fastjson.JSON;
import com.learning.springmvc.advance.valid.group.Insert;
import com.learning.springmvc.advance.valid.group.Select;
import com.learning.springmvc.advance.valid.validator.UserConstraint;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * ClassName: User
 * Description:
 * Date: 2016/12/22 21:02
 * <p>
 * 校验规则：
 * - @UserConstraint name 必须是eason
 * - name 属性不能为空
 * - mobile 在 Insert 分组时不能为空
 * - birthday 在 Select 分组时不能为空
 *
 * @author SAM SHO
 * @version V1.0
 */
@Setter
@Getter
//@UserConstraint
public class User {

    @NotNull(message = "{user.name.isNotNull}")
    private String name;

    @NotNull(groups = Insert.class)
    private String mobile;

    @NotNull(groups = Select.class)
    private Date birthday;

    @NotNull(message = "{user.createDate.isNotNull}")
    private Date createDate;

    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}
