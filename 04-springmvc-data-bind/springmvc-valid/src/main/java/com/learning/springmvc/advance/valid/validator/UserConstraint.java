package com.learning.springmvc.advance.valid.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * ClassName: UserConstraint
 * Description: 自定义校验注解
 * Date: 2019/5/22 13:57 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@Target({ElementType.TYPE, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {UserValidator.class})
public @interface UserConstraint {

    String message() default "{com.learning.springmvc.advance.validator.constraints.UserConstraint.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}