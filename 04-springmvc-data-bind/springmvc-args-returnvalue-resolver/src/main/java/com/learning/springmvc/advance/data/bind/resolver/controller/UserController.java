package com.learning.springmvc.advance.data.bind.resolver.controller;

import com.learning.springmvc.advance.data.bind.resolver.bean.User;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.format.datetime.DateFormatter;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * ClassName: UserController
 * Description:
 * Date: 2019/5/21 16:23 【需求编号】
 *
 * @author Sam Sho
 * @version V1.0.0
 */
@RestController
@RequestMapping("user")
public class UserController {

    /**
     * http://localhost:8080/user/date?date=2017-02-27
     * 报错：
     * 参数解析器为：RequestParamMethodArgumentResolver
     * 方法数据绑定的类型转换器为：SimpleTypeConverter
     * <p>
     * 添加属性格式化 @InitBinder + DateFormatter
     * 参数解析器为：RequestParamMethodArgumentResolver
     * 方法数据绑定的类型转换器为
     * @param date
     * @return
     */
    @GetMapping("date")
    public Date get(Date date) {
        return date;
    }


    /**
     * http://localhost:8080/user/get?name=sam&mobile=1234
     * <p>
     * <p>配置参数转换器
     * http://localhost:8080/user/get?name=sam&mobile=1234&birthday=2017-02-27 会报错（Date 类型转换）
     * <p>
     * 自定义参数转换器：
     * 使用 StringToUserConverter 后可以这样访问 http://localhost:8080/user/get?user=sam:1234
     * 使用 StringToUserConverter 后 http://localhost:8080/user/get?user=sam:1234:2017-02-27
     *
     * @param user
     * @return
     */
    @GetMapping("get")
    public User get(User user) {
        return user;
    }


    /**
     * 属性编辑器实现：
     * Spring MVC 在支持新的转换器框架的同时，也支持JavaBeans 的 PropertyEditor
     * <p>
     * <p>
     * 注解  @InitBinder 在控制器初始化时调用
     * <p>
     * WebDataBinder 处理请求消息和处理入参的绑定工作
     * <p>
     * 如果希望在全局范围内使用该编辑器，
     * 则可实现 org.springframework.web.bind.support.WebBindingInitializer 接口，并在 RequestMappingHandlerAdapter 中装配
     *
     * @param binder
     */
//    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
    }


    /**
     * 指定格式化程序
     *
     * @param binder
     */
    @InitBinder
    protected void initBinder2(WebDataBinder binder) {
        binder.addCustomFormatter(new DateFormatter("yyyy-MM-dd"));
    }

    /*
     * 对于同一个对象类型的类型转换以及属性编辑器，优先级顺序如下：
     *
     * 1- @InitBinder 装配自定义编辑器
     * 2- ConversionService 中装配了自定义转换器
     * 3- WebBindingInitializer 装配了自定义编辑器
     *
     */


}
